<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dersler extends Model
{
    protected $table = 'dersler';
    protected $guarded =['id'];

    public function ogrenciler()
    {
        return $this->belongsTo(Ogrenci::class,'ogrenci_id');
    }
}
