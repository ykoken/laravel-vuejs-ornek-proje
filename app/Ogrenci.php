<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ogrenci extends Model
{
    protected $table ='ogrenciler';
    protected $guarded = ['id'];

    public function dersler()
    {
        return $this->hasMany('App\Dersler');
    }
}
