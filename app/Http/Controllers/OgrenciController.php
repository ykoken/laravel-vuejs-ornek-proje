<?php

namespace App\Http\Controllers;

use App\Ogrenci;
use Illuminate\Http\Request;

class OgrenciController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Ogrenci  $ogrenci
     * @return \Illuminate\Http\Response
     */
    public function show(Ogrenci $ogrenci)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Ogrenci  $ogrenci
     * @return \Illuminate\Http\Response
     */
    public function edit(Ogrenci $ogrenci)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Ogrenci  $ogrenci
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Ogrenci $ogrenci)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Ogrenci  $ogrenci
     * @return \Illuminate\Http\Response
     */
    public function destroy(Ogrenci $ogrenci)
    {
        //
    }
}
