<?php

namespace App\Http\Controllers;

use App\Dersler;
use Illuminate\Http\Request;

class DerslerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Dersler  $dersler
     * @return \Illuminate\Http\Response
     */
    public function show(Dersler $dersler)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Dersler  $dersler
     * @return \Illuminate\Http\Response
     */
    public function edit(Dersler $dersler)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Dersler  $dersler
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Dersler $dersler)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Dersler  $dersler
     * @return \Illuminate\Http\Response
     */
    public function destroy(Dersler $dersler)
    {
        //
    }
}
