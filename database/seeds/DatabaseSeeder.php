<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('ogrenciler')->insert([
            'ogrenci_adi' => Str::random(10),
            'ogrenci_no' => Str::random(10).'@gmail.com',
        ]);
    }
}
