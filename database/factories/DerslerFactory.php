<?php

use Faker\Generator as Faker;

$factory->define(App\Dersler::class, function (Faker $faker) {
    return [
        'ogrenci_id' => $factory->create(App\Ogrenci::class)->id,
        'ders_adi' => $faker->sentence,
    ];
});
